"""
Software para descargar estados diarios de la corte suprema

    * Ingresar a https://oficinajudicialvirtual.pjud.cl/home/index.php#
    * Cerrar el dialogo
    * Al Apretar "Otros Servicios", escoger "Consulta de Causa
    * Ir a Estado Diario de Tribunales
    * En corte Suprema, buscar fecha ingresada
    * Guardar n° de Ingreso (Rol-Año)
        * Filtrar por:
            Tipo Recurso: (Civil)
            (opcional) Caratulado: No descargar ISAPRES o A.F.P
    * Ir a consulta unificada
    * En busqueda por RIT,
        Compentencia: Corte Suprema
        Tipo Busqueda: Recurso Corte Suprema
        
    * Por cada causa encontrada
        Ingresar Rol
        Ingresar Año
        Apretar buscar
        Apretar lupa
        Buscar
        Descargar todos los documentos que tengan Fecha Tramite la misma fecha escogida
            poner en nombre del documento el Tramite (Sentencia, Resolucion, etc)
            (opcional) No descargar Otro Tramite

"""
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common import exceptions
import wget_custom as wget
from datetime import datetime, timedelta
import time 
import os
import utils
from fallo_data import Fallo
from enum import Enum
import urllib.parse
import sys

url_web = "https://oficinajudicialvirtual.pjud.cl/home/index.php#"

ISAPRES_ARRAY = ["ISAPRE", "NUEVA MÁS VIDA", "CONSALUD", "CRUZ BLANCA", "COLMENA GOLDEN CROSS", "VIDA TRES", "BANMEDICA", "MODELO"]
DOCS_ARRAY = ["Resolución", "Sentencia"]

# TODO: add debug print
SUBSTRACT_DAYS_FROM_TODAY = 1
downloadFolder = "downloads"
fileFolder = "files"

def load_page(driver):
    if not os.path.exists(downloadFolder):
        os.mkdir(downloadFolder)
        print("Carpeta " + downloadFolder + " creada")
    else:
        print("Carpeta " + downloadFolder + " ya existe")

    driver.maximize_window()
    driver.get(url_web)
    print(driver.title)

    wait = WebDriverWait(driver, 10)

    if driver.title == "404 Not Found":
        print("Página no encontrada")
        return None

    print("Página encontrada")

    ############## Apretar "Consulta de Causas" #############
    consulta_causa_button = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.ID, "focus"))
                )
    consulta_causa_button_children = consulta_causa_button.find_elements_by_xpath(".//*")

    for element in consulta_causa_button_children:
        if element.get_attribute("tabindex") == '5':
            driver.execute_script(element.get_attribute("onclick"));
            break
    ############## Ir a Estado Diario de Tribunales #############
    sidebar = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.ID, "sidebar"))
                )
    driver.execute_script("consultaEstadoDiarioCompetencia();")
                

    ############## ESCOGER tab Busqueda por Fecha #############
    # nuevo_colapsador = WebDriverWait(driver, 10).until(
    #                 EC.presence_of_element_located((By.ID, "nuevocolapsador"))
    #             )
    # tab = nuevo_colapsador.find_element_by_xpath('//a[@href="#BusFecha"]')
    # tab.click()
    # time.sleep(1)
    return True

def get_date():
    ############## Pedir fecha a usuario ##############
    currentDT = datetime.now()
    date2check = currentDT - timedelta(days=SUBSTRACT_DAYS_FROM_TODAY)
    date_name = date2check.strftime("%d/%m/%Y")

    input_text = "Fecha a buscar: " + date_name + "\n"
    input_text += "(1) OK\n"
    input_text += "(2) Ingresar otra fecha\n"
    input_index = input(input_text)

    isInputOk = True
    if input_index == "1":
        pass
    elif input_index == "2":
        input_text = "Ingresa fecha en el siguiente formato: %d/%m/%Y -> ej: 19/05/2019\n"
        input_date = input(input_text)
        try:
            date2check = datetime.strptime(input_date, "%d/%m/%Y")
            date_name = input_date
        except ValueError as ex:
            print(str(ex))
            isInputOk = False
    else:
        print("Ingreso " + input_index + " no es una opcion valida")
        print("Cerrando programa")
        isInputOk = False


    if isInputOk:
        print("La fecha escogida es: " + date_name)
        return date_name
    else:
        return ""

def navegate_to_date(driver, date_name):
    date = date_name

    ############## ESCOGER FECHA #############
    date_input = WebDriverWait(driver, 5).until(
                    EC.element_to_be_clickable((By.ID, "fechaEstDiaSup"))
                )
    date_input.clear()          
    date_input.send_keys(date)
    date_input.click()

    ############## Apretar boton buscar #############
    search_button = WebDriverWait(driver, 5).until(
                    EC.element_to_be_clickable((By.ID, "btnConsultaEstDiaSuprema"))
                )
    search_button.click()
    time.sleep(2)

def calc_pages_number(driver):
    pages_number = 0

    # Obtener script para ir a la ultima pagina
    try:
        result_table = driver.find_element_by_id("resultConsultaEstDiaSuprema")
        table_rows_elements = result_table.find_element_by_tag_name("tbody").find_elements_by_tag_name("tr")
        pagination_array = table_rows_elements[-1].find_element_by_tag_name("ul").find_elements_by_tag_name("li")
        fin_button = pagination_array[-1].find_element_by_tag_name("span")
        text_to_search = str(fin_button.get_attribute("onclick"))

        # Obtener X (ultima pagina) del formato -> pagina(X,1);
        abre_parentesis_index = text_to_search.find("(")
        coma_index = text_to_search.find(",")
        pages_number = int(text_to_search[abre_parentesis_index + 1 : coma_index])
        print("Se encontraron " + str(pages_number) + " paginas en esta fecha\n")
    except exceptions.NoSuchElementException:
        pass    
    return pages_number

def get_fallos_list(driver, pages_number):
    n_fallos_total = 0
    fallo_list_total = []
    n_page = 1

    while n_page <= pages_number:
        next_button = None
        fallo_list = []

        ############## Encontrar los fallos de la página #############
        result_table = driver.find_element_by_id("resultConsultaEstDiaSuprema")
        table_rows_elements = result_table.find_element_by_tag_name("tbody").find_elements_by_tag_name("tr")

        i = 0
        n_fallos = len(table_rows_elements) - 1
        n_fallos_total += n_fallos

        print("Página " + str(n_page) + " de " + str(pages_number))

        for row in table_rows_elements:
            if i < n_fallos:
                row_elements = row.find_elements_by_tag_name("td")
                n_ingreso_array = str(row_elements[0].text).split("-")
                tipo_recurso = str(row_elements[1].text)
                caratulado_element = str(row_elements[2].text)
                if caratulado_element != None and caratulado_element != "" and \
                    "(Civil)" in tipo_recurso and \
                    all(isapre not in caratulado_element for isapre in ISAPRES_ARRAY):
                    print("N° de Ingreso: " + n_ingreso_array[0] + "-" + n_ingreso_array[1])
                    #TODO: revisar que fallo no exista
                    fallo_list.append(Fallo(n_ingreso_array[0], n_ingreso_array[1], caratulado_element))
        
            i += 1
        
        fallo_list_total.extend(fallo_list) 
        print("")
        
        ## Ir a la siguiente pagina
        n_page += 1
        if n_page <= pages_number:
            driver.execute_script("paginaSig(" + str(n_page - 1) + ",1);")
            time.sleep(2)
    
    print("\nLas causas encontradas son: " + str(n_fallos_total))
    print("\nLas causas filtradas son: " + str(len(fallo_list_total)))
    print("")
        
    return (n_fallos_total, fallo_list_total)

def get_download_link(driver, fallos_list, date_name):
    fallos_with_issues = []

    driver.execute_script("consultaUnificada();")
    time.sleep(2)
    form_busqueda_rit = driver.find_element_by_id("busRit").find_element_by_id("formConsulta")
    select_competencia_options = form_busqueda_rit.find_element_by_id("competencia").find_elements_by_tag_name("option")
    for option in select_competencia_options:
        if option.get_attribute("value") == 1:
            option.click()

    select_tipo_busqueda_options = form_busqueda_rit.find_element_by_id("conTipoBus").find_elements_by_tag_name("option")
    for option in select_tipo_busqueda_options:
        if option.get_attribute("value") == 0:
            option.click()

    i = 0
    fallos_total = len(fallos_list)

    for fallo in fallos_list:
        i += 1
        print("Causa " + str(i) + " de " + str(fallos_total) + ". " + str(fallo))
        rol_input = driver.find_element_by_id("conRolCausa")
        rol_input.clear()
        rol_input.send_keys(fallo.rol)

        año_input = driver.find_element_by_id("conEraCausa")
        año_input.clear()
        año_input.send_keys(fallo.año)

        search_button = WebDriverWait(driver, 5).until(
                    EC.element_to_be_clickable((By.ID, "btnConConsulta"))
                )
        search_button.click()

        ########### Seleccionar la "lupa", esperar a que modal se abra
        isLupaFound = False
        while not isLupaFound:
            try:
                result_table = WebDriverWait(driver, 10).until(
                            EC.visibility_of_element_located((By.ID, "resultConsulta"))
                        )
                table_row_element = WebDriverWait(result_table, 10).until(
                        EC.visibility_of_element_located((By.TAG_NAME, "tbody"))
                    ).find_element_by_tag_name("tr")
            
                lupa = table_row_element.find_element_by_tag_name("td")
                WebDriverWait(lupa, 10).until(
                            EC.element_to_be_clickable((By.TAG_NAME, "a"))
                        ).click()
                isLupaFound = True
            except exceptions.StaleElementReferenceException:
                print('StaleElementReferenceException, trying again')
                
            except exceptions.TimeoutException:
                print("Timed out waiting lupa. Causa no encontrada")
                break

        if not isLupaFound:
            fallos_with_issues.append(fallo)
            continue
                        
        #### Seleccionar modal y tabla       
        try:
            modal_detalle_fallo = WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located((By.ID, "modalDetalleSuprema"))
            )
        except exceptions.TimeoutException:
            print("Timed out waiting for modal_detalle_fallo to load")
            fallos_with_issues.append(fallo)
            continue

        try:
            table_modal = WebDriverWait(modal_detalle_fallo, 5).until(
                EC.visibility_of_element_located((By.ID, "movimientosSup"))
            ).find_element_by_tag_name("table")

            table_modal_rows = table_modal.find_element_by_tag_name("tbody").find_elements_by_tag_name("tr")
        except exceptions.TimeoutException:
            print("Timed out waiting for table_modal to load")
            error_modal_close_button = driver.find_element_by_xpath("//button[@class='confim']")
            error_modal_close_button.click()
            fallos_with_issues.append(fallo)
            continue

        ##### obtener urls de descarga
        n_total_docs = len(table_modal_rows)
        n_element = 0

        for row_element in table_modal_rows:
            try:
                n_element += 1
                tds = row_element.find_elements_by_tag_name("td")
                fecha_tramite = str(tds[4].text)
                doc_name = str(tds[5].text)

                if fecha_tramite == date_name and doc_name in DOCS_ARRAY:
                    download_doc_element_1 = row_element.find_element_by_tag_name("form")
                    doc_download_url = download_doc_element_1.get_attribute("action")
                    download_doc_element_2 = row_element.find_element_by_tag_name("input")
                    doc_download_url = doc_download_url + "?valorFile=" + download_doc_element_2.get_attribute("value")
                    print("\tDoc " + str(n_element) + " de " + str(n_total_docs) + " filtrado")
                    print("\t\tNombre: " + doc_name)
                    fallo.add_doc_data(doc_name, doc_download_url)
            except exceptions.NoSuchElementException as e:
                print("element " + str(n_element) + " no fue encontrado")
                print(str(e))
        
        print("")

        ### cerrar modal
        try:
            modal_close_button = WebDriverWait(modal_detalle_fallo, 5).until(
                EC.element_to_be_clickable((By.XPATH, "//button[@class='btn btn-default']"))
            )
            modal_close_button.click()
        except exceptions.TimeoutException:
            print("Timed out waiting for modal_close_button to load")
            continue

    if len(fallos_with_issues) > 0:
        print("No se pudo encontrar la información de " + str(len(fallos_with_issues)) + " causas")
        for fallo in fallos_with_issues:
            print(str(fallo))
    
    print("")

    return fallos_with_issues

def download_fallos(date_name, fallo_list):
    date_name_path = date_name.replace("/", "_")
    folderName = os.path.join(downloadFolder, date_name_path)
    fileFolderName = os.path.join(folderName, fileFolder)

    if not os.path.exists(folderName):
        os.mkdir(folderName)
        print("Carpeta " + folderName + " creada")
    else:
        print("Carpeta " + folderName + " ya existe")

    if not os.path.exists(fileFolderName):
        os.mkdir(fileFolderName)
        print("Carpeta " + fileFolderName + " creada")
    else:
        print("Carpeta " + fileFolderName + " ya existe")

    n_download_docs = 0
    for fallo in fallo_list:
        for i in range(len(fallo.doc_download_url_list)):
            url_fallo = fallo.doc_download_url_list[i] 
            if url_fallo != None:
                file_path = os.path.join(
                    fileFolderName, date_name_path + "&" + fallo.rol + "-" + fallo.año + "&" + str(i+1) + "&" + fallo.doc_name_list[i])
                print("\n" + file_path)
                file_complete_path = file_path + ".pdf"

                if os.path.exists(file_complete_path):
                    os.remove(file_complete_path)
                
                file_name = wget.download(url_fallo, file_complete_path)
                n_download_docs += 1
    
    print("\nDescarga Terminada")
    print("")
    return n_download_docs

def show_results(date_name, pages_number, n_fallos_total, fallos_list, fallos_with_issues, n_download_docs):
    text = "################# RESULTADOS ######################\n"
    text += "Se buscaron las causas para la fecha " + date_name + ".\n"
    text += "Para esta fecha se navegó por " + str(pages_number) + " páginas, encontrando " + str(n_fallos_total) + " causas en total.\n"
    text += "De las " + str(n_fallos_total) + " causas, " + str(len(fallos_list)) + " cumplieron con los filtros de búsqueda.\n"

    if len(fallos_with_issues) > 0:
        text += "Al tratar de descargar los documentos asociados a las " + str(len(fallos_list)) + "causas, " + str(len(fallos_with_issues)) + " se tuvo problemas al descargar.\n"
        text += "La información de las causas con problemas son:\n"
        i = 0
        for fallo in fallos_with_issues:
            i += 1
            print("Causa " + str(i) + ". " + str(fallo))
            print("")
    else:
        text += "Se encontró la informacion de descarga de todas las causas.\n"

    text += "Se descargaron un total de " + str(n_download_docs) + " documentos para las " + str(len(fallos_list)) + " causas.\n"
    text += "###################################################"
    print(text)

def close(driver):
    driver.close()
    sys.exit()

if __name__ == "__main__":
    print("Iniciando búsqueda y descarga de causas")
    driver = webdriver.Chrome("./chromedriver")
    if load_page(driver) == None:
        print("No se pudo cargar la pagina. Revisar url e intentar nuevamente")
        close(driver)

    date_name = get_date()
    if date_name == "":
        print("Error al obtener fecha del usuario. Intente nuavemente")
        close(driver)

    navegate_to_date(driver, date_name)

    print("Obteniendo cantidad de paginas para la fecha seleccioanada")
    pages_number = calc_pages_number(driver)
    if pages_number == 0:
        print("No hay causas para esta fecha")
        close(driver)

    print("Obteniendo Lista de Causas")
    (n_fallos_total, fallos_list) = get_fallos_list(driver, pages_number) 
    fallos_with_issues = get_download_link(driver, fallos_list, date_name)
    driver.close()

    if fallos_list == None or len(fallos_list) == 0:
        print("Error al tratar de obtener la lista de fallos")
        sys.exit()
    
    print("Descargando documentos de causas")
    n_download_docs = download_fallos(date_name, fallos_list)
    show_results(date_name, pages_number, n_fallos_total, fallos_list, fallos_with_issues, n_download_docs)
