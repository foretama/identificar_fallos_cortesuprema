
class Fallo():

    def __init__(self, rol, año, caratulado):
        self.rol = rol
        self.año = año
        self.caratulado = caratulado
        self.doc_name_list = []
        self.doc_download_url_list = []

    def add_doc_data(self, doc_name, new_url):
        self.doc_name_list.append(doc_name)
        self.doc_download_url_list.append(new_url)

    def __str__(self):
        text = "Su información es:\n"
        text += "\trol: " + self.rol + "\n"
        text += "\taño: " + self.año + "\n"
        text += "\tcaratulado: " + self.caratulado
        return text